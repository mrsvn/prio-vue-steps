import Vue from 'vue'
import VueRouter from 'vue-router'
import step1 from '@view/step1.vue'
import step2 from '@view/step2.vue'
import step3 from '@view/step3.vue'
import step4 from '@view/step4.vue'
import step5 from '@view/step5.vue'
import step6 from '@view/step6.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'step1',
    component: step1
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   component: about
  // },
  // {
  //   path: '/form',
  //   name: 'form',
  //   component: Form
  // },
  // {
  //   path: '/step1',
  //   name: 'step1',
  //   component: step1
  // },
  {
    path: '/step2',
    name: 'step2',
    component: step2
  },
  {
    path: '/step3',
    name: 'step3',
    component: step3
  },
  {
    path: '/step4',
    name: 'step4',
    component: step4
  },
  {
    path: '/step5',
    name: 'step5',
    component: step5
  },
  {
    path: '/step6',
    name: 'step6',
    component: step6
  },
  {
    path: '/consumerCredit',
    name: 'consumerCredit',
    component: consumerCredit
  }
]

const router = new VueRouter({
  mode: 'abstract',
  // base: process.env.BASE_URL,
  routes
})


export default router
