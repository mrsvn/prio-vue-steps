import Vue from 'vue'
import Vuex from 'vuex'
import {getField, updateField} from 'vuex-map-fields'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    creditTypeOptions: ['Потребительские цели', 'Развитие бизнеса', 'Автокредит', 'Ипотека', 'Рефинансирование'],
    mail: '',
    fio: '',
    number: '',
    amount: 3000000,
    creditTerm: 12,
    priceIsShown: true,
    creditType: 'Потребительские цели',
    checkboxChecked: true,
    checkedSalaryInPrio: false,
    checkedIpoteka: false,
    checkedGosuslugi: false,
    checkedCreditInPrio: false,
    consumerRate: '300000',

    carPriceRub: 2500000,
    own: 35000,
    housePriceRub: 2000000,
    firstPay: 40000,
    checkboxMaternity: false,
    checkboxIsCustomerCredit: false,
    checkboxIsIpoteka: false,
    checkboxIsAutoCredit: false,
    checkboxIsCreditCard: false,
    checkboxIsMicrozaym: false,
    checkboxOtherCredit: false,
    otherCreditType: '',
    otherCreditSum: 2500000,
    refinanceSum: 100000,
    needForAdditionalFunds: true,


    checkedOptions: [{
      value: 'Potrebitelsky',
      text: 'Потребительский'
    },
      {
        value: 'Autocredit',
        text: 'Автокредит'
      },
      {
        value: 'Microzaym',
        text: 'Микрозайм'
      },
      {
        value: 'Ipoteka',
        text: 'Ипотека'
      },
      {
        value: 'CreditCard',
        text: 'Кредитная карта'
      },
      {
        value: 'Other',
        text: 'Другой'
      }
    ],

    checkedOption: '',


    birthPlace: '',
    passportNum: '',
    birthDate: '',
    passportDate: '',
    passportCode: '',
    passportGivenBy: '',
    familyStatus: 'Холост, не замужем',
    familyStatusOptions: ['Холост, не замужем', 'Гражданский брак', 'Женат, замужем', 'Разведен, разведена', 'Вдовец, вдова'],
    permanentAddressIndex: '',
    permanentAddressRegion: '',
    permanentAddressCity: '',
    permanentAddressLocality: '',
    permanentAddressStreet: '',
    permanentAddressHouse: '',
    permanentAddressCorpus: '',
    permanentAddressStroenie: '',
    permanentAddressFlat: '',
    checkboxEqualsToPermanentAddress: true,
    actualAddressIndex: '',
    actualAddressRegion: '',
    actualAddressCity: '',
    actualAddressLocality: '',
    actualAddressStreet: '',
    actualAddressHouse: '',
    actualAddressCorpus: '',
    actualAddressStroenie: '',
    actualAddressFlat: '',

    smsCode: '',

    education: 'Начальное',
    educationOptions: ['Начальное', 'Среднее', 'Среднее-специальное', 'Неполное высшее', 'Высшее', 'Второе высшее или ученая степень'],
    work: 'Работаю в организации',
    workOptions: ['Работаю в организации', 'Собственный бизнес', 'Учащийся', 'Пенсионер', 'Другое'],
    orgName: 'KFC',
    orgNameOptions: ['KFC', 'Макдоналдс', 'Старбакс', 'Бургер Кинг', 'СМС кафе'],
    position: 'Индивидуальный предприниматель',
    positionOptions: ['Индивидуальный предприниматель', 'Владелец фирмы', 'Адвокат', 'Нотариус', 'Другое'],
    workExperience: '6 месяцев и более',
    workExperienceOptions: ['6 месяцев и более', 'от 6 месяцев до 1 года', 'от 1 до 3 лет', 'от 3 до 5 лет', 'более 5 лет'],
    salaryBank: 'Прио-Внешторг банк',
    salaryBankOptions: ['Прио-Внешторг банк', 'Сбербанк', 'ВТБ', 'Промсвязьбанк', 'Альфа Банк', 'Другой банк', 'Не получаю зарплату на карту'],
    familyMembersNum: 'Один',
    familyMembersNumOptions: ['Нет', 'Один', 'Двое', 'Трое', 'Четверо', 'Пятеро и более'],
    extraIncome: 'Индивидуальный предприниматель',
    extraIncomeOptions: ['Индивидуальный предприниматель', 'Работа по совместительству', 'Доход от собственного бизнеса', 'Стипендия', 'Пенсия', 'Другое'],
    inn: '',
    workIndex: '',
    workRegion: '',
    workCity: '',
    workLocality: '',
    workStreet: '',
    workHouse: '',
    workCorpus: '',
    workStroenie: '',
    workOffice: '',
    workNumber: '',
    workAdditionalNumber: '',
    averageIncome: '',
    snils: '',
    additionalWorkName: '',
    additionalInn: '',
    checkboxWorkEqualsToLivingAddress: false,
    checkboxSecondWorkEqualsToLivingAddress: true,


    secondWorkIndex: '',
    secondWorkRegion: '',
    secondWorkCity: '',
    secondWorkLocality: '',
    secondWorkStreet: '',
    secondWorkHouse: '',
    secondWorkCorpus: '',
    secondWorkStroenie: '',
    secondWorkOffice: '',
    secondWorkNumber: '',
    secondWorkAdditionalNumber: '',
    secondPosition: 'Индивидуальный предприниматель',
    secondPositionOptions: ['Индивидуальный предприниматель', 'Владелец фирмы', 'Адвокат', 'Нотариус', 'Другое'],
    secondWorkExperience: '6 месяцев и более',
    secondWorkExperienceOptions: ['6 месяцев и более', 'от 6 месяцев до 1 года', 'от 1 до 3 лет', 'от 3 до 5 лет', 'более 5 лет'],
    secondSalaryBank: 'Прио-Внешторг банк',
    secondSalaryBankOptions: ['Прио-Внешторг банк', 'Сбербанк', 'ВТБ', 'Промсвязьбанк', 'Альфа Банк', 'Другой банк', 'Не получаю зарплату на карту'],


    checkboxHaveFlat: false,
    propertyType: 'Жилое помещение',
    propertyTypeOptions: ['Жилое помещение', 'Нежилое помещение', 'Загородный дом'],
    propertyIndex: '',
    propertyRegion: '',
    propertyCity: '',
    propertyLocality: '',
    propertyStreet: '',
    propertyHouse: '',
    propertyCorpus: '',
    propertyStroenie: '',
    propertyFlat: '',
    propertySquare: '',
    propertyPrice: '',
    propertyShare: '',
    checkboxHaveNonResidentialProperty: false,

    nonResPropertyType: 'Нежиилое помещение',
    nonResPropertyTypeOptions: ['Жилое помещение', 'Нежилое помещение', 'Загородный дом'],
    nonResPropertyIndex: '',
    nonResPropertyRegion: '',
    nonResPropertyCity: '',
    nonResPropertyLocality: '',
    nonResPropertyStreet: '',
    nonResPropertyHouse: '',
    nonResPropertyCorpus: '',
    nonResPropertyStroenie: '',
    nonResPropertyFlat: '',
    nonResPropertySquare: '',
    nonResPropertyPrice: '',
    nonResPropertyShare: '',

    checkboxHaveCar: false,
    carType: 'Легковое',
    carTypeOptions: ['Легковое', 'Грузовое'],
    carModel: '',
    carYear: '',
    carPrice: '',
    carNumber: '',
    checkboxHaveOSAGO: false,
    checkboxHaveCASCO: false,
    checkboxHaveOtherProperty: false,
    otherPropertyDescription: '',

    checkboxFlatAsCollateral: true,
    checkboxNonResidentalAsCollateral: true,
    checkboxAutoAsCollateral: true,
    checkboxOtherAsCollateral: true,


    city: 'москва',

    activeStep: 1,


    schema: {
      steps: [
        {
          text: 'Условия кредита'
        },
        {
          text: 'Персональные данные'
        },
        {
          text: 'Работа и доходы'
        },
        {
          text: 'Сведения об имуществе'
        },

      ],
      mail: {
        name: 'mail',
        mask: '',
        label: 'Ваш E-mail',
        validateRules: {
          required: true,
          regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        }
      },
      fio: {
        name: 'fio',
        label: 'Фамилия, имя и отчество<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      number: {
        placeholder: '+ 7 (980) 693 79 09',
        name: 'number',
        label: 'Ваш номер телефона<sup>*</sup>',
        mask: '+7 (999) 999 99 99',
        validateRules: {
          required: true,
          regex: /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/
        }
      },
      amount: {
        placeholder: '2 000 000',
        name: 'amount',
        label: 'Сумма кредита',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      otherCreditSum: {
        placeholder: '2 000 000',
        name: 'otherCreditSum',
        label: 'Общий остаток в других банках',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      refinanceSum: {
        placeholder: '100 000',
        name: 'refinanceSum',
        label: 'Сумма',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      creditTerm: {
        placeholder: '1 год',
        name: 'creditTerm',
        label: 'Срок кредита',
      },

      otherCreditType: {
        placeholder: 'Другой',
        name: 'otherCreditType',
        label: 'Другой',
      },


      carPriceRub: {
        placeholder: '2 000 000',
        name: 'carPriceRub',
        label: 'Стоимость автомобиля',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      own: {
        placeholder: '35 000',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        name: 'own',
        label: 'Собственные средства',
      },

      housePriceRub: {
        placeholder: '2 000 000',
        name: 'carPriceRub',
        label: 'Стоимость недвижимости',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      firstPay: {
        placeholder: '35 000',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        name: 'own',
        label: 'Первоначальный взнос',
      },


      birthPlace: {
        name: 'birthPlace',
        label: 'Место рождения<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      passportNum: {
        name: 'passportNum',
        label: 'Серия и номер<sup>*</sup>',
        mask: '9999 999999',
        validateRules: {
          required: true
        }
      },

      birthDate: {
        name: 'birthDate',
        label: 'Дата рождения<sup>*</sup>',
        mask: '99.99.9999',
        validateRules: {
          required: true
        }
      },

      passportDate: {
        name: 'passportDate',
        label: 'Дата выдачи<sup>*</sup>',
        mask: '99.99.9999',
        validateRules: {
          required: true
        }
      },
      passportCode: {
        name: 'passportCode',
        label: 'Код подразделения<sup>*</sup>',
        mask: '999-999',
        validateRules: {
          required: true
        }
      },
      passportGivenBy: {
        name: 'passportGivenBy',
        label: 'Кем выдан<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      permanentAddressIndex: {
        name: 'permanentAddressIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },

      permanentAddressRegion: {
        name: 'permanentAddressRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      permanentAddressCity: {
        name: 'permanentAddressCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      permanentAddressLocality: {
        name: 'permanentAddressLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      permanentAddressStreet: {
        name: 'permanentAddressStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      permanentAddressHouse: {
        name: 'permanentAddressHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      permanentAddressCorpus: {
        name: 'permanentAddressCorpus',
        label: 'Корпус',
        mask: '',
      },

      permanentAddressStroenie: {
        name: 'permanentAddressStroenie',
        label: 'Строение',
        mask: '',
      },

      permanentAddressFlat: {
        name: 'permanentAddressFlat',
        label: 'Квартира',
        mask: '',
      },

      actualAddressIndex: {
        name: 'actualAddressIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },

      actualAddressRegion: {
        name: 'actualAddressRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      actualAddressCity: {
        name: 'actualAddressCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      actualAddressLocality: {
        name: 'actualAddressLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      actualAddressStreet: {
        name: 'actualAddressStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      actualAddressHouse: {
        name: 'actualAddressHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      actualAddressCorpus: {
        name: 'actualAddressCorpus',
        label: 'Корпус',
        mask: '',
      },

      actualAddressStroenie: {
        name: 'actualAddressStroenie',
        label: 'Строение',
        mask: '',
      },

      actualAddressFlat: {
        name: 'actualAddressFlat',
        label: 'Квартира',
        mask: '',
      },


      smsCode: {
        name: 'smsCode',
        // label: 'Квартира<sup>*</sup>',
        mask: '9999',
        validateRules: {
          required: true
        }
      },

      inn: {
        name: 'inn',
        label: 'ИНН',
        mask: ''
      },
      workIndex: {
        name: 'workIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },
      workRegion: {
        name: 'workRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      workCity: {
        name: 'workCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      workLocality: {
        name: 'workLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      workStreet: {
        name: 'workStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      workHouse: {
        name: 'workHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      workCorpus: {
        name: 'workCorpus',
        label: 'Корпус',
        mask: ''
      },
      workStroenie: {
        name: 'workStroenie',
        label: 'Строение',
        mask: ''
      },
      workOffice: {
        name: 'workOffice',
        label: 'Офис',
        mask: ''
      },

      workAdditionalNumber: {
        name: 'workAdditionalNumber',
        label: 'Добавочный номер',
        mask: ''
      },

      snils: {
        name: 'snils',
        label: 'СНИЛС',
        mask: '999-999-999-99'
      },

      additionalWorkName: {
        name: 'additionalWorkName',
        label: 'Название организации',
        mask: ''
      },
      additionalInn: {
        name: 'additionalWorkInn',
        label: 'ИНН',
        mask: ''
      },

      workNumber: {
        name: 'workNumber',
        label: 'Рабочий телефон<sup>*</sup>',
        mask: '+7 (999) 999 99 99',
        validateRules: {
          required: true
        }
      },
      averageIncome: {
        name: 'averageIncome',
        label: 'Средний доход семьи в месяц после вычета налогов<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },




      secondWorkIndex: {
        name: 'secondWorkIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },
      secondWorkRegion: {
        name: 'secondWorkRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      secondWorkCity: {
        name: 'secondWorkCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      secondWorkLocality: {
        name: 'secondWorkLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      secondWorkStreet: {
        name: 'secondWorkStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      secondWorkHouse: {
        name: 'secondWorkHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      secondWorkCorpus: {
        name: 'secondWorkCorpus',
        label: 'Корпус',
        mask: ''
      },
      secondWorkStroenie: {
        name: 'secondWorkStroenie',
        label: 'Строение',
        mask: ''
      },
      secondWorkOffice: {
        name: 'secondWorkOffice',
        label: 'Офис',
        mask: ''
      },

      secondWorkNumber: {
        name: 'secondWorkNumber',
        label: 'Рабочий телефон<sup>*</sup>',
        mask: '+7 (999) 999 99 99',
        validateRules: {
          required: true
        }
      },

      secondWorkAdditionalNumber: {
        name: 'secondWorkAdditionalNumber',
        label: 'Добавочный номер',
        mask: ''
      },


      additionalsecondWorkName: {
        name: 'additionalsecondWorkName',
        label: 'Название организации',
        mask: ''
      },


      propertyIndex: {
        name: 'propertyIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },

      propertyRegion: {
        name: 'propertyRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      propertyCity: {
        name: 'propertyCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      propertyLocality: {
        name: 'propertyLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      propertyStreet: {
        name: 'propertyStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      propertyHouse: {
        name: 'propertyHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      propertyCorpus: {
        name: 'propertyCorpus',
        label: 'Корпус',
        mask: '',
      },

      propertyStroenie: {
        name: 'propertyStroenie',
        label: 'Строение',
        mask: '',
      },

      propertyFlat: {
        name: 'propertyFlat',
        label: 'Квартира',
        mask: '',
      },

      propertySquare: {
        name: 'propertySquare',
        label: 'Плошадь, кв.м',
        mask: '',
      },

      propertyPrice: {
        name: 'propertyPrice',
        label: 'Стоимость',
        mask: '',
      },

      propertyShare: {
        name: 'propertyShare',
        label: 'Доля в праве собственности',
        mask: '',
      },




      nonResPropertyIndex: {
        name: 'nonResPropertyIndex',
        label: 'Индекс',
        mask: '999999',
        validateRules: {
          required: true
        }
      },

      nonResPropertyRegion: {
        name: 'nonResPropertyRegion',
        label: 'Регион<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      nonResPropertyCity: {
        name: 'nonResPropertyCity',
        label: 'Район или город<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      nonResPropertyLocality: {
        name: 'nonResPropertyLocality',
        label: 'Населенный пункт<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },
      nonResPropertyStreet: {
        name: 'nonResPropertyStreet',
        label: 'Улица<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      nonResPropertyHouse: {
        name: 'nonResPropertyHouse',
        label: 'Дом<sup>*</sup>',
        mask: '',
        validateRules: {
          required: true
        }
      },

      nonResPropertyCorpus: {
        name: 'nonResPropertyCorpus',
        label: 'Корпус',
        mask: '',
      },

      nonResPropertyStroenie: {
        name: 'nonResPropertyStroenie',
        label: 'Строение',
        mask: '',
      },

      nonResPropertyFlat: {
        name: 'nonResPropertyFlat',
        label: 'Квартира',
        mask: '',
      },

      nonResPropertySquare: {
        name: 'nonResPropertySquare',
        label: 'Плошадь, кв.м',
        mask: '',
      },

      nonResPropertyPrice: {
        name: 'nonResPropertyPrice',
        label: 'Стоимость',
        mask: '',
      },

      nonResPropertyShare: {
        name: 'nonResPropertyShare',
        label: 'Доля в праве собственности',
        mask: '',
      },

      carModel: {
        name: 'carModel',
        label: 'Марка и модель',
        mask: '',
      },

      carYear: {
        name: 'carYear',
        label: 'Год выпуска',
        mask: '',
      },
      carPrice: {
        name: 'carPrice',
        label: 'Cтоимость',
        mask: '',
      },
      carNumber: {
        name: 'carNumber',
        label: 'Госномер',
        mask: '',
      },
      otherPropertyDescription: {
        name: 'otherPropertyDescription',
        label: 'Опишите обект собственности',
        mask: '',
      },

    }
  },
  getters: {
    getField,
    getPropertyType: state => state.propertyType
  },
  mutations: {
    updateField,
    setData(state, payload) {
      const {amount, consumerRate, creditTerm} = payload
      state.amount = amount;
      state.consumerRate = consumerRate;
      state.creditTerm = creditTerm;
    },
    setPropertyType(state, payload) {
      state.propertyType = payload
    }
  },
  actions: {
    async getData({getters, commit}) {
      const data = {
        amount: 3000000,
        consumerRate: 9.9,
        creditTerm: 15
      }
      commit('setData', data)
    },
    setPropertyType({commit}, payload) {
      commit('setPropertyType', payload)
    }
  },
  modules: {}
})
