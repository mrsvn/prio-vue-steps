import Vue from 'vue'
// import './plugins/axios'
// import validation from
import App from './App.vue'
import router from './router'
import store from './store'



var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo, {
  offset:-90
})

import { ValidationProvider, ValidationObserver, extend, localize } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

extend('required', {
  ...required,
  message:'Обязательное поле'
});

import { setInteractionMode } from 'vee-validate';

setInteractionMode('lazy');



// import ru from './assets/dictonary/ru/';

import * as rules from "vee-validate/dist/rules";
import ru from "vee-validate/dist/locale/ru.json";


Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize("ru", ru);

localize({ru: {
        names: {
          password: 'пароль',
          email: 'email',
          tel: 'телефон'
        }
      }});




import RangeSlider from 'vue-range-slider'
import 'vue-range-slider/dist/vue-range-slider.css';

import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
Vue.component('vSelect', vSelect);

Vue.component('AppInput', AppInput);
import AppInput from '@/components/Input.vue';


Vue.component('PriceInput', PriceInput);
import PriceInput from '@/components/PriceInput.vue'


import AnimatedNumber from '@/components/AnimatedNumber.vue';

Vue.component('RangeSlider', RangeSlider);


Vue.filter(
    'priceFormat',
    (value, currency) => {
      if (value) {
        return `${value.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₽`
      }

    }

);

import 'normalize-scss'
import "@styles/main.scss"
Vue.config.productionTip = false

Vue.component('animated-integer', AnimatedNumber)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#bottom')
